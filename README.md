# HSE Generic Networking Lib

The _DQMH Generic Networking Module_ is a DQMH Singleton module altered to allow for (near) zero-coupled networking functionality.
